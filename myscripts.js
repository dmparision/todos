console.log('myscripts is loading . . .');

//validate form & print
function printData(){

	//alert('validating & printing Data . . .');

	if(confirm("Are you sure to Validate & print the Data ??")){

		//get name 
		var name = document.getElementById('name').value;
		//get email 
		var email = document.getElementById('email').value;
		//get mobile   
		var mobile = document.getElementById('mobile').value;
		//get country 
		var country = document.getElementById('country').value;
		//get qualification chebox 
		const checkboxes = document.querySelectorAll('input[name="qualification"]:checked');
		let qualification = [];
		
		checkboxes.forEach((checkbox) => {
			qualification.push(checkbox.value);
		});
		if (qualification.length > 0) qualification = qualification.toString();

		//get gender 
		var radios = document.querySelectorAll('input[type="radio"]:checked');
		var gender = radios.length>0? radios[0].value: null;

		//console.log(radios.length);
		//console.log(qualification.length);

		//do the validation    
		if( checkName(name) && checkEmail(email) && checkMobileNumber(mobile) && chekQualification(qualification) && checkCountry(country) && checkGender(radios) ){
		    
		    //pass all validation

		    console.log(name+","+email+","+mobile+","+country);
		    //create html
		    var rowCount = document.getElementById('userData').rows.length;
		    var table = document.getElementById( 'userData' ),
		    row = table.insertRow(-1),
		    cell0 = row.insertCell(0),
		    cell1 = row.insertCell(1);
		    cell2 = row.insertCell(2);
		    cell3 = row.insertCell(3);
		    cell4 = row.insertCell(4);
		    cell5 = row.insertCell(5);
		    cell6 = row.insertCell(6);

		    cell0.innerHTML = rowCount;
		    cell1.innerHTML = name;
		    cell2.innerHTML = email;
		    cell3.innerHTML = mobile;
		    cell4.innerHTML = qualification;
		    cell5.innerHTML = country;
		    cell6.innerHTML = gender;

		    //reset form data
		    document.getElementById('userForm').reset();
		}else{

		    console.log('Data has Validation Error . . .');
		}
	}else{

	  console.log('Cancel Valida & Print Data. . .');
	}
}
//checkname
function checkName(str){
	let nameFormat = /^[a-zA-Z ]*$/;
	//console.log( nameFormat.test(str));
	if(str == ""){

	  alert("Name field is required .");      
	  return false;

	}else if(str.length < 3 ){

	  alert("Name field should have minimum 3 chars.");
	  return false;
	}else if( !nameFormat.test(str)){

	  alert("Name field should contains alphabates & space only.");
	  return false;
	}else if( str.length > 20 ){

	  alert("Name field should have maximium 15 chars.");
	  return false;
	}else{
	  return true;
	}
}
//check email
function checkEmail(str){
	let mailformat = /\S+@\S+\.\S+/;
	//console.log( mailformat.test(str));
	if(str == ""){
	  alert("Email field is required .");
	  return false;
	}else if( !mailformat.test(str) ){
	  alert("Email is not a valid format .");
	  return false;
	}else if(str.length >30 ){
	  alert("Email field should have maximium 30 chars.");
	  return false;
	}else{
	  return true;
	}
} 
//check mobile number
function checkMobileNumber(str){
	let mobileFormat = /^[0-9]*$/;
	//console.log( mobileFormat.test(str));

	if(str == ""){
	  alert("Mobile number is required .")
	  return false;
	}else if( !mobileFormat.test(str)){
	  alert("Mobile number contains numeric digits only .")
	  return false;
	}else if(str.length < 10 || str.length > 12){
	  alert("Mobile number should have 10 to 12 digits ")
	  return false;
	}else {
	  return true;
	}
}

//check checkbox input
function chekQualification(str){

	if(str == ""){
	  alert("Please check at least one Educational qualification .");
	  return false;

	}else{
	  return true;
	}
}

//check select input
function checkCountry(str){

	if(str == ""){
	  alert("Please select one country .");
	  return false;
	  
	}else{
	  return true;
	}

}

//check radio input
function checkGender(str){

	if(str.length == 0){
	  alert("Please select Gender .");      
	  return false;
	  
	}else{
	  return true;
	}

}